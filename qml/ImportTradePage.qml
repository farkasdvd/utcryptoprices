/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3

import CoinGecko 1.0 // Trades

Page {
    id: _root
    header: PageHeader {
        id: _header
        title: i18n.tr("Import trades")
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Help")
                iconName: "help"

                onTriggered: {
                    PopupUtils.open(Qt.resolvedUrl("ImportHelpDialog.qml"), _root)
                }
            }
        ]
    }

    property var activeTransfer

    ContentPeerPicker {
        anchors.topMargin: _header.height
        showTitle: false
        contentType: ContentType.Text
        handler: ContentHandler.Source

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single
            _root.activeTransfer = peer.request()
        }
    }

    ContentTransferHint {
        anchors.fill: parent
        activeTransfer: _root.activeTransfer
    }

    Connections {
        target: _root.activeTransfer

        onStateChanged: {
            if(_root.activeTransfer.state == ContentTransfer.Charged) {
                var contentItem = _root.activeTransfer.items[0];

                if(Trades.importTrades(contentItem.url)) {
                    pageStack.pop()
                }
                else {
                    PopupUtils.open(Qt.resolvedUrl("ImportFailedDialog.qml"), _root)
                }
            }
        }
    }
}

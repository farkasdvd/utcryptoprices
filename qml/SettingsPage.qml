/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import CoinGecko 1.0 // Currencies

Page {
    id: _root

    header: PageHeader {
        id: _header
        title: i18n.tr("Settings")
    }

    Flickable {
        id: _body
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        contentHeight: _content.height

        Column {
            id: _content
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right

            GeneralListItem {
                id: _defaultFiat
                titleText: i18n.tr("Default fiat currency")
                subtitleText: _defaultFiat.currency.name
                iconSource: _defaultFiat.currency.icon
                action: Action {
                    onTriggered: {
                        var popup = PopupUtils.open(Qt.resolvedUrl("EditCurrencyDialog.qml"), _root,
                                    { oldCurrency: _defaultFiat.currency, filter: true, filterType: 2 })

                        popup.selected.connect(function(newCurrency) {
                            settings.defaultFiatCurrencyId = newCurrency.id
                        })
                    }
                }

                property var currency: Currencies.get(settings.defaultFiatCurrencyId)
            }

            ListItem {
                id: _theme
                height: _themeContent.height

                readonly property string systemThemeName: ""
                readonly property string ambianceThemeName: "Ubuntu.Components.Themes.Ambiance"
                readonly property string suruDarkThemeName: "Ubuntu.Components.Themes.SuruDark"

                SlotsLayout {
                    id: _themeContent
                    mainSlot: OptionSelector {
                        id: _themeSelector
                        text: i18n.tr("App theme")
                        model: [
                            i18n.tr("System"),
                            i18n.tr("Ambiance"),
                            i18n.tr("SuruDark")
                        ]

                        onSelectedIndexChanged: {
                            settings.appThemeName = (_themeSelector.selectedIndex == 1) ? _theme.ambianceThemeName :
                                                    (_themeSelector.selectedIndex == 2) ? _theme.suruDarkThemeName : _theme.systemThemeName
                        }

                        Component.onCompleted: {
                            _themeSelector.selectedIndex = (settings.appThemeName == _theme.ambianceThemeName) ? 1 :
                                                             (settings.appThemeName == _theme.suruDarkThemeName) ? 2 : 0
                        }
                    }
                }
            }

            GeneralListItem {
                id: _about
                titleText: i18n.tr("Report an issue")
                subtitleText: "Gitlab"
                iconName: "external-link"
                action: Action {
                    onTriggered: {
                        Qt.openUrlExternally("https://gitlab.com/farkasdvd/utcryptoprices")
                    }
                }
            }

            GeneralListItem {
                id: _donate
                titleText: i18n.tr("Donate")
                subtitleText: "Bitcoin"
                iconSource: "assets/donate.png"
                action: Action {
                    onTriggered: {
                        PopupUtils.open(Qt.resolvedUrl("DonationDialog.qml"), _root)
                    }
                }
            }
        }
    }
}

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3

import CoinGecko 1.0 // Currencies

Dialog {
    id: _root
    title: i18n.tr("Select currency")

    property var oldCurrency
    property bool filter: false
    property int filterType

    signal selected(var newCurrency)

    OptionSelector {
        id: _currencyList
        expanded: true
        model: Currencies
        delegate: OptionSelectorDelegate {
            enabled: !(_root.filter && (_root.filterType != currency.type))
            text: currency.name
            iconSource: currency.icon
            constrainImage: true
            action: Action {
                onTriggered: {
                    _root.selected(currency)
                    PopupUtils.close(_root)
                }
            }
        }

        Component.onCompleted: {
            console.log("%1 * %2".arg(Currencies.rowCount()).arg(itemHeight))
            containerHeight = Math.min(units.gu(30), Currencies.rowCount() * itemHeight)

            for(var i = 0; i < _currencyList.model.rowCount(); ++i) {
                if(_root.oldCurrency.id == _currencyList.model.at(i).id) {
                    _currencyList.selectedIndex = i
                    break
                }
            }
        }
    }
}

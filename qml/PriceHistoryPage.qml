/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3

Page {
    id: _root

    header: PageHeader {
        id: _header
        title: _root.tradePair.currency.name

        trailingActionBar.actions: [
            Action {
                iconName: "reload"
                enabled: !_root.tradePair.downloading

                onTriggered: {
                    _root.tradePair.downloadHistory()
                }
            }
        ]

        extension: Sections {
            id: _sections
            actions: [
                Action {
                    text: i18n.tr("Last day")

                    onTriggered: {
                        _canvas.historyPortion = 0.034
                        _canvas.calculateMinMax()
                        _canvas.requestPaint()
                    }
                },
                Action {
                    text: i18n.tr("Last week")

                    onTriggered: {
                        _canvas.historyPortion = 0.25
                        _canvas.calculateMinMax()
                        _canvas.requestPaint()
                    }
                },
                Action {
                    text: i18n.tr("Last month")

                    onTriggered: {
                        _canvas.historyPortion = 1
                        _canvas.calculateMinMax()
                        _canvas.requestPaint()
                    }
                }
            ]
        }
    }

    property var tradePair

    Connections {
        target: _root.tradePair

        onHistoryChanged: {
            _canvas.calculateMinMax()
            _canvas.requestPaint()
        }
    }

    GeneralListItem {
        id: _info
        anchors.top: _header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        titleText: "%1 %2".arg(_canvas.priceAtPointer).arg(_root.tradePair.vsCurrency.symbol)
        subtitleText: Qt.formatDateTime(_canvas.timeAtPointer, Qt.LocalDate)
        iconSource: _root.tradePair.currency.icon
        isProgressionVisible: false
    }

    Icon {
        id: _loading
        anchors.top: _info.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        visible: _root.tradePair.downloading
        name: "transfer-progress-download"
    }

    Canvas {
        id: _canvas
        anchors.top: _info.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        visible: !_root.tradePair.downloading

        property double historyPortion: 1
        property int historyLength: Math.floor(_root.tradePair.historyLength * _canvas.historyPortion)
        property double leftMargin: units.dp(2)
        property double topMargin: units.dp(4)
        property double axisXSegmentLength: (_canvas.width - _canvas.leftMargin) / (_canvas.historyLength - 1)
        property int minPriceIndex: _root.tradePair.historyLength - 1
        property int maxPriceIndex: _root.tradePair.historyLength - 1
        property double priceAtPointer: _root.tradePair.price
        property var timeAtPointer: _root.tradePair.lastUpdate

        function addPointer(mouseX) {
            var pointerX = mouseX
            var i = Math.floor(mouseX / _canvas.axisXSegmentLength)

            if(i < 0) {
                i = 0
            }
            else if(i >= _canvas.historyLength) {
                i = _canvas.historyLength - 1
            }

            var historyIndex = _root.tradePair.historyLength - (_canvas.historyLength - i)
            var point1X = i * _canvas.axisXSegmentLength

            // last point (i+1 is out of bound)
            if(i == (_canvas.historyLength - 1))
            {
                pointerX = point1X

                _canvas.timeAtPointer = _root.tradePair.timeAt(historyIndex)
                _canvas.priceAtPointer = _root.tradePair.fixedPriceAt(historyIndex)
            }
            else {
                var point2X = point1X + _canvas.axisXSegmentLength

                // check which point is closer
                if(Math.abs(point1X - mouseX) <= Math.abs(point2X - mouseX)) {
                    pointerX = point1X

                    _canvas.timeAtPointer = _root.tradePair.timeAt(historyIndex)
                    _canvas.priceAtPointer = _root.tradePair.fixedPriceAt(historyIndex)
                }
                else {
                    pointerX = point2X
                    _canvas.timeAtPointer = _root.tradePair.timeAt(historyIndex + 1)
                    _canvas.priceAtPointer = _root.tradePair.fixedPriceAt(historyIndex + 1)
                }
            }

            return pointerX
        }

        function calculateMinMax() {
            _canvas.minPriceIndex = _root.tradePair.historyLength - 1
            _canvas.maxPriceIndex = _root.tradePair.historyLength - 1

            for(var i = 0; i < _canvas.historyLength; ++i) {
                var historyIndex = _root.tradePair.historyLength - i - 1

                if(_root.tradePair.priceAt(historyIndex) < _root.tradePair.priceAt(_canvas.minPriceIndex)) {
                    _canvas.minPriceIndex = historyIndex
                }

                if(_root.tradePair.priceAt(historyIndex) > _root.tradePair.priceAt(_canvas.maxPriceIndex)) {
                    _canvas.maxPriceIndex = historyIndex
                }
            }
        }

        onPaint: {
            console.log("historyLength=%1".arg(_canvas.historyLength))

            if(_canvas.historyLength > 0) {
                var ctxt = _canvas.getContext("2d")
                ctxt.reset(0, 0, _canvas.width, _canvas.height)

                ctxt.strokeStyle = theme.palette.normal.foregroundText
                ctxt.fillStyle = theme.palette.normal.foreground
                ctxt.lineWidth = units.dp(4)

                ctxt.beginPath()
                ctxt.moveTo(0, _canvas.height)
                // the graph is painted from the right side
                ctxt.lineTo(_canvas.width, _canvas.height)

                var d = (_canvas.height - topMargin) / (_root.tradePair.priceAt(_canvas.maxPriceIndex) - _root.tradePair.priceAt(_canvas.minPriceIndex))
                for(var i = 0; i < _canvas.historyLength; ++i) {
                    var y = topMargin + (d * (_root.tradePair.priceAt(_canvas.maxPriceIndex) - _root.tradePair.priceAt(_root.tradePair.historyLength - i - 1)))
                    ctxt.lineTo(_canvas.width - (i * _canvas.axisXSegmentLength), y)
                }
                ctxt.moveTo(_canvas.leftMargin, _canvas.height)

                ctxt.stroke()
                ctxt.fill()
            }
            else {
                console.log("Nothing to paint")
            }
        }

        MouseArea {
            anchors.fill: parent

            onPressed: {
                _pointer.x = _canvas.addPointer(mouse.x)
                _pointer.visible = true
                mouse.accepted = true
            }
            onPositionChanged: {
                _pointer.x = _canvas.addPointer(mouse.x)
            }
            onReleased: {
                _canvas.priceAtPointer = _root.tradePair.price
                _canvas.timeAtPointer = _root.tradePair.lastUpdate
                _pointer.visible = false
            }
        }
    }

    Rectangle {
        id: _pointer
        anchors.top: _info.bottom
        anchors.bottom: parent.bottom
        width: units.dp(4)
        visible: false
        color: theme.palette.normal.focus
    }

    Component.onCompleted: {
        _root.tradePair.downloadHistory()
    }
}

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: _root
    title: i18n.tr("Import failed")

    Label {
        text: i18n.tr("For further details check the software logs.")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        text: i18n.tr("Check the rules")

        onClicked: {
            PopupUtils.open(Qt.resolvedUrl("ImportHelpDialog.qml"), _root)
        }
    }

    Button {
        text: i18n.tr("Close")

        onClicked: {
            PopupUtils.close(_root)
        }
    }
}

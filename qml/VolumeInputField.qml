/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3

import CoinGecko 1.0 // Currency

Row {
    id: _root
    height: _volume.height
    spacing: units.gu(1)

    property var currency
    property alias currencyEditEnabled: _currency.enabled
    property alias volume: _volume.text
    property alias volumePlaceholder: _volume.placeholderText

    signal currencyClicked()

    Button {
        id: _currency
        width: units.gu(6)
        height: parent.height
        text: _root.currency.symbol
        action: Action {
            onTriggered: {
                _root.currencyClicked()
            }
        }
    }

    TextField {
        id: _volume
        width: parent.width - _currency.width - parent.spacing
        placeholderText: "0.0"
        inputMethodHints: Qt.ImhFormattedNumbersOnly | Qt.ImhNoPredictiveText
        validator: DoubleValidator {
            bottom: 0.0
            decimals: 10
        }
    }
}

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3

import CoinGecko 1.0 // CryptoPrices

UbuntuListView {
    id: _root
    anchors.fill: parent
    pullToRefresh.enabled: true
    pullToRefresh.refreshing: CryptoPrices.updating
    pullToRefresh.onRefresh: CryptoPrices.update()
    currentIndex: -1
    clip: true
    model: CryptoPrices
    delegate: GeneralListItem {
        titleText: "%1 %2".arg(tradePair.price).arg(tradePair.vsCurrency.symbol)
        subtitleText: "%1%".arg(tradePair.priceShift.toFixed(2))
        iconSource: tradePair.currency.icon
        action: Action {
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("PriceHistoryPage.qml"), { tradePair: tradePair })
            }
        }
    }

    Component.onCompleted: {
        CryptoPrices.initialize(settings.defaultFiatCurrencyId)
    }
}

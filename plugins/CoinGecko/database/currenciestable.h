/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CURRENCIESTABLE_H
#define CURRENCIESTABLE_H

#include "table.h"

class Currency;

class CurrenciesTable: public Table
{
public:
    CurrenciesTable();
    ~CurrenciesTable();

    bool initialize() override;
    bool addRow(const QVariantList& values) override;
    bool updateRow(const QVariantList& values) override;
    bool deleteRow(const QVariant& curreny_id) override;
    bool load() override;
    Currency* getCurrency(const QString& id) const;
    QList<Currency*> getCurrencies() const;
    QList<Currency*> getCryptoCurrencies() const;
    QList<Currency*> getFiatCurrencies() const;

private:
    QList<Currency*> m_currencies;
};

#endif // CURRENCIESTABLE_H

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "currency.h"

Currency::Currency()
    : Currency(QString(), QString(), QString(), QString(), Currency::TypeUndefined)
{
    // intentionally empty
}

Currency::Currency(const QString& id,
                   const QString& name,
                   const QString& symbol,
                   const QString& icon,
                   const int type)
    : QObject()
    , m_id(id)
    , m_name(name)
    , m_symbol(symbol)
    , m_icon(icon)
    , m_type(static_cast<Currency::Type>(type))
{
    // intentionally empty
}

const QString& Currency::id() const
{
    return m_id;
}

void Currency::setId(const QString& id)
{
    if(id != m_id)
    {
        m_id = id;
        emit idChanged();
    }
}

const QString& Currency::name() const
{
    return m_name;
}

void Currency::setName(const QString& name)
{
    if(name != m_name)
    {
        m_name = name;
        emit nameChanged();
    }
}

const QString& Currency::symbol() const
{
    return m_symbol;
}

void Currency::setSymbol(const QString& symbol)
{
    if(symbol != m_symbol)
    {
        m_symbol = symbol;
        emit symbolChanged();
    }
}

const QString& Currency::icon() const
{
    return m_icon;
}

void Currency::setIcon(const QString& icon)
{
    if(icon != m_icon)
    {
        m_icon = icon;
        emit iconChanged();
    }
}

int Currency::type() const
{
    return m_type;
}

void Currency::setType(const int type)
{
    if(type != m_type)
    {
        m_type = static_cast<Currency::Type>(type);
        emit typeChanged();
    }
}

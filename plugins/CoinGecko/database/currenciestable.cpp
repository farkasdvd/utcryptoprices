/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "currenciestable.h"
#include "currency.h"

#include <QDebug>
#include <QQmlEngine>

CurrenciesTable::CurrenciesTable()
    : Table("currencies", {
          Column{"id", "text primary key"},
          Column{"name", "text"},
          Column{"symbol", "text"},
          Column{"icon", "text"},
          Column{"type", "integer"}
      })
    , m_currencies()
{
    // intentionally empty
}

CurrenciesTable::~CurrenciesTable()
{
    for(Currency* currency: m_currencies)
    {
        delete currency;
    }
}

bool CurrenciesTable::initialize()
{
    bool result = true;

    result = result && addRow({"usd", "US Dollar", "$", "assets/usd.svg", Currency::TypeFiat});
    result = result && addRow({"eur", "Euro", "€", "assets/eur.svg", Currency::TypeFiat});
    result = result && addRow({"gbp", "Great British Pound", "£", "assets/gbp.svg", Currency::TypeFiat});
    result = result && addRow({"czk", "Czech Crown", "Kč", "assets/czk.svg", Currency::TypeFiat});
    result = result && addRow({"huf", "Hungarian Forint", "Ft", "assets/huf.svg", Currency::TypeFiat});
    result = result && addRow({"cad", "Canadian Dollar", "C$", "assets/cad.svg", Currency::TypeFiat});
    result = result && addRow({"aud", "Australian Dollar", "A$", "assets/aud.svg", Currency::TypeFiat});
    result = result && addRow({"chf", "Swiss Franc", "CHF", "assets/chf.svg", Currency::TypeFiat});
    result = result && addRow({"jpy", "Japanese Yen", "¥", "assets/jpy.svg", Currency::TypeFiat});
    result = result && addRow({"bitcoin", "Bitcoin", "BTC", "assets/btc.svg", Currency::TypeCrypto});
    result = result && addRow({"ethereum", "Ethereum", "ETH", "assets/eth.svg", Currency::TypeCrypto});
    result = result && addRow({"litecoin", "Litecoin", "LTC", "assets/ltc.svg", Currency::TypeCrypto});

    if(result)
    {
        qDebug() << "Initialized table" << name();
    }

    return result;
}

bool CurrenciesTable::addRow(const QVariantList& values)
{
    bool result = false;

    if(values.size() == columnCount())
    {
        const QString query_template = "insert into %1 values (?, ?, ?, ?, ?)";
        QSqlQuery query;

        query.prepare(query_template.arg(name()));
        query.addBindValue(values.at(0).toString());
        query.addBindValue(values.at(1).toString());
        query.addBindValue(values.at(2).toString());
        query.addBindValue(values.at(3).toString());
        query.addBindValue(values.at(4).toInt());

        result = execute(query);
    }
    else
    {
        qDebug() << "Columns vs values mismatch";
    }

    return result;
}

bool CurrenciesTable::updateRow(const QVariantList& values)
{
    bool result = false;

    qDebug() << "Yet to be implemented";

    return result;
}

bool CurrenciesTable::deleteRow(const QVariant& curreny_id)
{
    bool result = false;

    qDebug() << "Yet to be implemented";

    return result;
}

bool CurrenciesTable::load()
{
    bool result = false;
    const QString query_template = "select * from %1";
    QSqlQuery query;

    query.prepare(query_template.arg(name()));
    m_currencies.clear();

    if(execute(query))
    {
        qDebug() << "Loaded table" << name();

        while(query.next())
        {
            const QString id = query.value(0).toString();
            const QString name = query.value(1).toString();
            const QString symbol = query.value(2).toString();
            const QString icon = query.value(3).toString();
            const int type = query.value(4).toInt();

            Currency* currency = new Currency(id, name, symbol, icon, type);
            QQmlEngine::setObjectOwnership(currency, QQmlEngine::CppOwnership);

            m_currencies.push_back(currency);
        }

        result = true;
    }

    return result;
}

Currency* CurrenciesTable::getCurrency(const QString& id) const
{
    Currency* result = nullptr;

    for(Currency* currency : m_currencies)
    {
        if(currency->id() == id)
        {
            result = currency;
            break;
        }
    }

    return result;
}

QList<Currency*> CurrenciesTable::getCurrencies() const
{
    return m_currencies;
}

QList<Currency*> CurrenciesTable::getCryptoCurrencies() const
{
    QList<Currency*> result;

    for(Currency* currency : m_currencies)
    {
        if(currency->type() == Currency::TypeCrypto)
        {
            result.push_back(currency);
        }
    }

    return result;
}

QList<Currency*> CurrenciesTable::getFiatCurrencies() const
{
    QList<Currency*> result;

    for(Currency* currency : m_currencies)
    {
        if(currency->type() == Currency::TypeFiat)
        {
            result.push_back(currency);
        }
    }

    return result;
}

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATABASE_H
#define DATABASE_H

#include "currenciestable.h"
#include "tradestable.h"

#include <QObject>
#include <QSqlDatabase>
#include <QDir>
#include <QString>

class Currency;
class Trade;
class QDate;

class Database: public QObject
{
    Q_OBJECT

public:
    static Database* getInstance();

    ~Database();

    Currency* getCurrency(const QString& id) const;
    QList<Currency*> getCurrencies() const;
    QList<Currency*> getCryptoCurrencies() const;
    QList<Currency*> getFiatCurrencies() const;
    Trade* getTrade(const int id) const;
    QList<Trade*> getTrades() const;
    bool insertTrade(const QString& input_currency_id,
                     const double input_volume,
                     const QString& output_currency_id,
                     const double output_volume,
                     const double fee,
                     const QDate& date,
                     const QString& note);
    bool updateTrade(const int trade_id,
                     const QString& input_currency_id,
                     const double input_volume,
                     const QString& output_currency_id,
                     const double output_volume,
                     const double fee,
                     const QDate& date,
                     const QString& note);
    bool deleteTrade(const int trade_id);

signals:
    void tradesChanged();

private:
    Database();
    void prepareTables();

    QSqlDatabase m_db;
    const QString m_db_driver;
    const QDir m_db_dir;
    const QString m_db_name;
    CurrenciesTable m_currencies_table;
    TradesTable m_trades_table;
};

#endif // DATABASE_H

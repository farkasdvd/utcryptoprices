/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRADE_H
#define TRADE_H

#include <QObject>
#include <QString>
#include <QDate>

class Trade: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id NOTIFY idChanged)
    Q_PROPERTY(QString inputCurrencyId READ inputCurrencyId WRITE setInputCurrencyId NOTIFY inputCurrencyIdChanged)
    Q_PROPERTY(QString inputVolume READ fixedInputVolume NOTIFY inputVolumeChanged)
    Q_PROPERTY(QString inputVolumeWithFee READ fixedInputVolumeWithFee NOTIFY inputVolumeWithFeeChanged)
    Q_PROPERTY(QString outputCurrencyId READ outputCurrencyId WRITE setOutputCurrencyId NOTIFY outputCurrencyIdChanged)
    Q_PROPERTY(QString outputVolume READ fixedOutputVolume NOTIFY outputVolumeChanged)
    Q_PROPERTY(QString fee READ fixedFee NOTIFY feeChanged)
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QString note READ note WRITE setNote NOTIFY noteChanged)

public:
    Trade();
    Trade(const int id,
          const QString& input_currency_id,
          const double input_volume,
          const QString& output_currency_id,
          const double output_volume,
          const double fee,
          const QDate& date,
          const QString& note );

    int id() const;
    void setId(const int id);
    const QString& inputCurrencyId() const;
    void setInputCurrencyId(const QString& currency_id);
    double inputVolume() const;
    QString fixedInputVolume() const;
    QString fixedInputVolumeWithFee() const;
    void setInputVolume(const double volume);
    const QString& outputCurrencyId() const;
    void setOutputCurrencyId(const QString& currency_id);
    double outputVolume() const;
    QString fixedOutputVolume() const;
    void setOutputVolume(const double volume);
    double fee() const;
    QString fixedFee() const;
    void setFee(const double fee);
    const QDate& date() const;
    void setDate(const QDate& date);
    const QString& note() const;
    void setNote(const QString& note);

signals:
    void idChanged();
    void inputCurrencyIdChanged();
    void inputVolumeChanged();
    void inputVolumeWithFeeChanged();
    void outputCurrencyIdChanged();
    void outputVolumeChanged();
    void feeChanged();
    void dateChanged();
    void noteChanged();

private:
    int m_id;
    QString m_input_currency_id;
    double m_input_volume;
    QString m_output_currency_id;
    double m_output_volume;
    double m_fee;
    QDate m_date;
    QString m_note;
};

#endif // TRADE_H

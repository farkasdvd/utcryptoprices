/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cryptoprices.h"
#include "tradepair.h"
#include "database/currency.h"
#include "database/database.h"
#include "network/pricerequest.h"

#include <QDebug>
#include <QString>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QStringList>

CryptoPrices::CryptoPrices()
    : QAbstractListModel()
    , m_trade_pairs()
    , m_updating(false)
    , m_vs_currency_id()
    , m_network_manager()
{
    connect(&m_network_manager, &NetworkManager::priceRequestFinished, this, &CryptoPrices::handlePriceRequestFinished);
}

void CryptoPrices::initialize(const QString& vs_currency_id)
{
    setVsCurrencyId(vs_currency_id);

    if(m_trade_pairs.empty())
    {
        QList<Currency*> cryptos = Database::getInstance()->getCryptoCurrencies();

        if(!cryptos.empty())
        {
            Currency* const vs_currency = Database::getInstance()->getCurrency(m_vs_currency_id);

            beginInsertRows(QModelIndex(), 0, (cryptos.size() - 1));

            for(Currency* const crypto : cryptos)
            {
                m_trade_pairs.append(new TradePair(crypto, vs_currency));
            }

            endInsertRows();

            update();
        }
    }
    else
    {
        qDebug() << "Already initialized";
    }
}

void CryptoPrices::update()
{
    if(!updating())
    {
        if(!m_trade_pairs.empty())
        {
            QStringList crypto_ids;

            for(TradePair* trade_pair : m_trade_pairs)
            {
                crypto_ids.push_back(trade_pair->currency()->id());
            }

            setUpdating(m_network_manager.sendPriceRequest(crypto_ids, m_vs_currency_id));
        }
        else
        {
            qDebug() << "Nothing to update";
        }
    }
    else
    {
        qDebug() << "Already updating";
    }
}

QString CryptoPrices::getFixedValue(const QString& currency_id, const QString& volume) const
{
    double value = 0;

    for(const TradePair* trade_pair : m_trade_pairs)
    {
        if(currency_id == trade_pair->currency()->id())
        {
            value = trade_pair->price() * volume.toDouble();
            break;
        }
    }

    return QString::number(value, 'f', 2);
}

bool CryptoPrices::updating() const
{
    return m_updating;
}

void CryptoPrices::setUpdating(const bool updating)
{
    if(updating != m_updating)
    {
        m_updating = updating;
        emit updatingChanged();
    }
}

const QString& CryptoPrices::vsCurrencyId() const
{
    return m_vs_currency_id;
}

void CryptoPrices::setVsCurrencyId(const QString& id)
{
    if(id != m_vs_currency_id)
    {
        m_vs_currency_id = id;
        emit vsCurrencyIdChanged();

        Currency* const vs_currency = Database::getInstance()->getCurrency(m_vs_currency_id);

        for(TradePair* trade_pair : m_trade_pairs)
        {
            trade_pair->setVsCurrency(vs_currency);
        }
    }
}

int CryptoPrices::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_trade_pairs.size();
}

QHash<int, QByteArray> CryptoPrices::roleNames() const
{
    QHash<int, QByteArray> result;

    result[RoleTradePair] = "tradePair";

    return result;
}

QVariant CryptoPrices::data(const QModelIndex& index, const int role) const
{
    QVariant result;

    switch(role)
    {
        case RoleTradePair:
            result = QVariant::fromValue(m_trade_pairs.at(index.row()));
            break;
        default:
            qDebug() << "Role" << role << "is not supported";
            break;
    }

    return result;
}

void CryptoPrices::handlePriceRequestFinished(PriceRequest* request)
{
    qDebug() << "Price request finished";

    for(TradePair* trade_pair : m_trade_pairs)
    {
        const QString& currency_id = trade_pair->currency()->id();

        trade_pair->setLastUpdateToNow();
        trade_pair->setPrice(request->price(currency_id));
        trade_pair->setPriceShift(request->priceShift(currency_id));
    }

    setUpdating(false);
}

TradePair* CryptoPrices::findTradePair(const QString& currency_id)
{
    for(auto trade_pair : m_trade_pairs)
    {
        if(currency_id == trade_pair->currency()->id())
        {
            return trade_pair;
        }
    }

    return nullptr;
}

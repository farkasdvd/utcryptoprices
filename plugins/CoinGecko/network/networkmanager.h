/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include "pricerequest.h"
#include "historyrequest.h"

#include <QObject>
#include <QNetworkAccessManager>
#include <QQueue>

class QString;
class QStringList;

class NetworkManager: public QObject
{
    Q_OBJECT

public:
    NetworkManager();

    bool sendPriceRequest(const QStringList& currency_ids, const QString& vs_currency_id);
    bool sendHistoryRequest(const QString& currency_id, const QString& vs_currency_id);

signals:
    void priceRequestFinished(PriceRequest* request);
    void historyRequestFinished(HistoryRequest* request);

private slots:
    void handleRequestFinished(QNetworkReply* const reply);

private:
    QNetworkAccessManager m_manager;
    QQueue<Request*> m_requests;
    PriceRequest m_price_request;
    HistoryRequest m_history_request;
};

#endif // NETWORKMANAGER_H

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_H
#define REQUEST_H

#include <QObject>
#include <QNetworkRequest>
#include <QString>
#include <QJsonDocument>

class QByteArray;

class Request: public QObject
{
public:
    Request(const QString& url_template);

    bool finished() const;
    QString toString() const;
    void setResponse(const QByteArray& response);

    virtual const QNetworkRequest& request() = 0;

protected:
    bool m_finished;
    QString m_url_template;
    QNetworkRequest m_request;
    QJsonDocument m_response;
};

#endif // REQUEST_H

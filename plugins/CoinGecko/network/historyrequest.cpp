/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "historyrequest.h"

#include <QDebug>
#include <QJsonArray>

HistoryRequest::HistoryRequest()
    : Request("https://api.coingecko.com/api/v3/coins/%1/market_chart?vs_currency=%2&days=30&interval=hourly")
    , m_currency_id()
    , m_vs_currency_id()
{
    // intentionally empty
}

void HistoryRequest::setCurrencyId(const QString& currency_id)
{
    m_currency_id = currency_id;
}

void HistoryRequest::setVsCurrencyId(const QString& vs_currency_id)
{
    m_vs_currency_id = vs_currency_id;
}

const QNetworkRequest& HistoryRequest::request()
{
    m_finished = false;
    m_request.setUrl(m_url_template.arg(m_currency_id).arg(m_vs_currency_id));

    return m_request;
}

QList<QPair<QDateTime, double>> HistoryRequest::priceHistory() const
{
    QList<QPair<QDateTime, double>> result;

    if(!m_response.isNull())
    {
        if(m_response["prices"].isArray())
        {
            for(const auto& json_price : m_response["prices"].toArray())
            {
                const double epoch_time = json_price.toArray()[0].toDouble();
                const double price = json_price.toArray()[1].toDouble();

                result.push_back(qMakePair(QDateTime::fromMSecsSinceEpoch(epoch_time), price));
            }
        }
        else
        {
            qDebug() << "Price history not found in the response";
        }
    }
    else
    {
        qDebug() << "History request response is empty";
    }

    return result;
}

/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pricerequest.h"

#include <QDebug>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonValue>

PriceRequest::PriceRequest()
    : Request("https://api.coingecko.com/api/v3/simple/price?ids=%1&vs_currencies=%2&include_24hr_change=true")
    , m_currency_ids()
    , m_vs_currency_id()
{
    // intentionally empty
}

void PriceRequest::setCurrencyIds(const QStringList& currency_ids)
{
    m_currency_ids = currency_ids;
}

void PriceRequest::setVsCurrencyId(const QString& vs_currency_id)
{
    m_vs_currency_id = vs_currency_id;
}

const QNetworkRequest& PriceRequest::request()
{
    m_finished = false;
    m_request.setUrl(m_url_template.arg(m_currency_ids.join(",")).arg(m_vs_currency_id));

    return m_request;
}

double PriceRequest::price(const QString& currency_id) const
{
    double result = 0.0;

    if(!m_response.isNull())
    {
        if(m_response[currency_id].isObject() && !m_response[currency_id].toObject().keys().empty())
        {
            const QJsonValue json_price = m_response[currency_id][m_vs_currency_id];

            if(json_price.isDouble())
            {
                result = json_price.toDouble();
            }
            else
            {
                qDebug() << "Price in" << m_vs_currency_id << "not found in the response";
            }
        }
        else
        {
            qDebug() << "Currency" << currency_id << "received 0 prices";
        }
    }
    else
    {
        qDebug() << "Price request response is null";
    }

    return result;
}

double PriceRequest::priceShift(const QString& currency_id) const
{
    double result = 0.0;

    if(!m_response.isNull())
    {
        if(m_response[currency_id].isObject() && !m_response[currency_id].toObject().keys().empty())
        {
            const QJsonValue json_price_shift = m_response[currency_id][m_vs_currency_id + "_24h_change"];

            if(json_price_shift.isDouble())
            {
                result = json_price_shift.toDouble();
            }
            else
            {
                qDebug() << "Price shift in" << m_vs_currency_id << "not found in the response";
            }
        }
        else
        {
            qDebug() << "Currency" << currency_id << "received 0 price shifts";
        }
    }
    else
    {
        qDebug() << "Price request response is empty";
    }

    return result;
}

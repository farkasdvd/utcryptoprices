/*
 * Copyright (C) 2022  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "networkmanager.h"

#include <QDebug>
#include <QNetworkReply>

NetworkManager::NetworkManager()
    : m_manager()
    , m_requests()
    , m_price_request()
    , m_history_request()
{
    connect(&m_manager, &QNetworkAccessManager::finished, this, &NetworkManager::handleRequestFinished);
}

bool NetworkManager::sendPriceRequest(const QStringList& currency_ids, const QString& vs_currency_id)
{
    bool result = false;

    if(m_price_request.finished())
    {
        m_price_request.setCurrencyIds(currency_ids);
        m_price_request.setVsCurrencyId(vs_currency_id);
        m_requests.enqueue(&m_price_request);
        m_manager.get(m_price_request.request());

        qDebug() << "Price request sent:" << m_price_request.toString();

        result = true;
    }
    else
    {
        qDebug() << "Price request is already in progress";
    }

    return result;
}

bool NetworkManager::sendHistoryRequest(const QString& currency_id, const QString& vs_currency_id)
{
    bool result = false;

    if(m_history_request.finished())
    {
        m_history_request.setCurrencyId(currency_id);
        m_history_request.setVsCurrencyId(vs_currency_id);
        m_requests.enqueue(&m_history_request);
        m_manager.get(m_history_request.request());

        qDebug() << "History request sent:" << m_history_request.toString();

        result = true;
    }
    else
    {
        qDebug() << "History request is already in progress";
    }

    return result;
}

void NetworkManager::handleRequestFinished(QNetworkReply* const reply)
{
    Request* const request = m_requests.dequeue();

    if(request == &m_price_request)
    {
        m_price_request.setResponse(reply->readAll());

        emit priceRequestFinished(&m_price_request);
    }
    else if(request == &m_history_request)
    {
        m_history_request.setResponse(reply->readAll());

        emit historyRequestFinished(&m_history_request);
    }
    else
    {
        qDebug() << "Received unexpected reply";
    }

    reply->deleteLater();
}

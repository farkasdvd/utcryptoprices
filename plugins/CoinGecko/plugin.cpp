/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "cryptoprices.h"
#include "portfolio.h"
#include "currencies.h"
#include "trades.h"
#include "database/currency.h"

void ExamplePlugin::registerTypes(const char *uri) {
    qmlRegisterSingletonType<CryptoPrices>(uri, 1, 0, "CryptoPrices", [](QQmlEngine*, QJSEngine*) -> QObject* { return new CryptoPrices; });
    qmlRegisterSingletonType<Portfolio>(uri, 1, 0, "Portfolio", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Portfolio; });
    qmlRegisterSingletonType<Currencies>(uri, 1, 0, "Currencies", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Currencies; });
    qmlRegisterSingletonType<Trades>(uri, 1, 0, "Trades", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Trades; });
    qmlRegisterUncreatableType<Currency>(uri, 0, 1, "Currency", "Cannot create Currency from QML.");
}

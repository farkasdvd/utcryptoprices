/*
 * Copyright (C) 2021  David Farkas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utcryptoprices is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CRYPTOPRICES_H
#define CRYPTOPRICES_H

#include "network/networkmanager.h"

#include <QAbstractListModel>
#include <QList>
#include <QString>

class Currency;
class TradePair;
class PriceRequest;

class CryptoPrices: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool updating READ updating NOTIFY updatingChanged)
    Q_PROPERTY(QString vsCurrencyId READ vsCurrencyId WRITE setVsCurrencyId NOTIFY vsCurrencyIdChanged)

public:
    enum Role
    {
        RoleTradePair
    };

    CryptoPrices();

    Q_INVOKABLE void initialize(const QString& vs_currency_id);
    Q_INVOKABLE void update();
    Q_INVOKABLE QString getFixedValue(const QString& currency_id, const QString& volume) const;

    bool updating() const;
    void setUpdating(const bool updating);
    const QString& vsCurrencyId() const;
    void setVsCurrencyId(const QString& id);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex& index, const int role = Qt::DisplayRole) const override;

signals:
    void updatingChanged();
    void vsCurrencyIdChanged();

private slots:
    void handlePriceRequestFinished(PriceRequest* request);

private:
    TradePair* findTradePair(const QString& currency_id);

    QList<TradePair*> m_trade_pairs;
    bool m_updating;
    QString m_vs_currency_id;
    NetworkManager m_network_manager;
};

#endif // CRYPTOPRICES_H
